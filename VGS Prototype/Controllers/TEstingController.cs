﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VGS_Prototype.Controllers
{
    public class TestingController : Controller
    {
        private readonly ApplicationUserManager _userManager;

        public TestingController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }
        // GET: TEsting

        public TestingController()
        {
            
        }
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            var user = await _userManager.FindByIdAsync(User.Identity.Name);
            user.Claims.Add(new IdentityUserClaim(){ClaimType = "test",ClaimValue = "true",});
            return View();
        }

        // GET: TEsting/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TEsting/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TEsting/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TEsting/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TEsting/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TEsting/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TEsting/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
