﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VGS_Prototype.Models;

namespace VGS_Prototype.Controllers
{
    public class reCaptchaResponse
    {
        public bool success { get; set; }
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Article item;
            using (var db = new VGSWebEntities())
            {
                item = db.Articles.First(s => s.PageTitle == "Home");

                return View(item);

            }
        }

        public ActionResult WhatIsVGS()
        {
            Article item;
            using (var db = new VGSWebEntities())
            {
                item = db.Articles.First(s => s.PageTitle == "What is VGS");

            return View(item);

            }
        }

        public ActionResult GeneralFAQ()
        {
            Article item;
            using (var db = new VGSWebEntities())
            {
                item = db.Articles.First(s => s.PageTitle == "GeneralFaq");

                return View(item);

            }
        }

        public ActionResult SupportFAQ()
        {
            ViewBag.Title = "FAQ:General";
            return View();
        }

        public ActionResult SamplingMethods()
        {
            Article item;
            using (var db = new VGSWebEntities())
            {
                item = db.Articles.First(s => s.PageTitle == "SamplingMethods");

                return View(item);

            }
        }

        public ActionResult ContactUs()
        {
            ViewBag.Title = "Contact Us";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult VerifyKendo()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult TestInvisiblereCaptcha()
        {
            return View();


        }

        [HttpPost]
        public ActionResult TestInvisiblereCaptcha(string test)
        {
            var result = CheckCaptcha();
            return View();
        }

        private bool CheckCaptcha()
        {
            //http://www.thatsoftwaredude.com/content/6401/implementing-googles-invisible-recaptcha-in-net
            string url = "https://www.google.com/recaptcha/api/siteverify";
            WebRequest request = WebRequest.Create(url);
            string postData = string.Format("secret={0}&response={1}&remoteip={2}",
                "6LeCnIYUAAAAAALyBShbjV5hshQzsWB34TF6yb3F", Request["g-recaptcha-response"],
                Request.UserHostAddress);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postData.Length;

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(postData);
            writer.Close();

            StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());
            string responseData = reader.ReadToEnd();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            reCaptchaResponse cResponse = jss.Deserialize<reCaptchaResponse>(responseData);
            return cResponse.success;
        }
    }

}