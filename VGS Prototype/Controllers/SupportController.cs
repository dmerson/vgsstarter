﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VGS_Prototype.Controllers
{
    public class SupportController : Controller
    {
        // GET: Support
        public ActionResult General()
        {
            ViewBag.Title = "General Support";
            return View();
        }
        public ActionResult MyTickets()
        {
            ViewBag.Title = "My Tickets";
            return View();
        }
        public ActionResult VGSSupport()
        {
            ViewBag.Title = "VGS Support";
            return View();
        }
    }
}