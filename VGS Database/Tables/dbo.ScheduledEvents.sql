CREATE TABLE [dbo].[ScheduledEvents]
(
[ScheduledEventId] [int] NOT NULL IDENTITY(1, 1),
[Subject] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Start] [datetime] NOT NULL,
[End] [datetime] NOT NULL,
[RecurrenceRule] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecurrenceParentId] [int] NULL,
[RecurrenceException] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsAllDay] [bit] NOT NULL,
[OwnerId] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_ScheduleEvents_LastModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScheduledEvents] ADD CONSTRAINT [PK_ScheduleEvents] PRIMARY KEY CLUSTERED  ([ScheduledEventId]) ON [PRIMARY]
GO
