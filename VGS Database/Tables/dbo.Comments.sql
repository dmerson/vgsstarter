CREATE TABLE [dbo].[Comments]
(
[CommentId] [int] NOT NULL IDENTITY(1, 1),
[CommentFrom] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsRead] [bit] NULL CONSTRAINT [DF_Comments_IsRead] DEFAULT ((0)),
[LastModified] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Table_1_LastModified1] DEFAULT ('Entered by Customer'),
[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_Comments_LastModifiedDate] DEFAULT (getdate()),
[ReplyTo] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comments] ADD CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED  ([CommentId]) ON [PRIMARY]
GO
