CREATE TABLE [dbo].[SupportTickets]
(
[SupportTicketId] [int] NOT NULL IDENTITY(1, 1),
[TicketState] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TicketPriority] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssignedToEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Header] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssueDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatorEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedTime] [datetime] NOT NULL CONSTRAINT [DF_SupportTickets_CreatedTime] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SupportTickets] ADD CONSTRAINT [PK_SupportTickets] PRIMARY KEY CLUSTERED  ([SupportTicketId]) ON [PRIMARY]
GO
