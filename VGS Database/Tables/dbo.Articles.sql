CREATE TABLE [dbo].[Articles]
(
[ArticleId] [int] NOT NULL IDENTITY(1, 1),
[PageTitle] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArticleHtml] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastModified] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Articles_LastModified] DEFAULT ('Entered via Database'),
[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Articles_LastModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Articles] ADD CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED  ([ArticleId]) ON [PRIMARY]
GO
