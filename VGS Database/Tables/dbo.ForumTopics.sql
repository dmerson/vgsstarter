CREATE TABLE [dbo].[ForumTopics]
(
[ForumTopicId] [int] NOT NULL IDENTITY(1, 1),
[Topic] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ForumTopics] ADD CONSTRAINT [PK_ForumTopics] PRIMARY KEY CLUSTERED  ([ForumTopicId]) ON [PRIMARY]
GO
