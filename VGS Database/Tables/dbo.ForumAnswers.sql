CREATE TABLE [dbo].[ForumAnswers]
(
[ForumAnswerId] [int] NOT NULL IDENTITY(1, 1),
[ForumQuestionId] [int] NOT NULL,
[Answer] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatorEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastModified] [datetime] NOT NULL CONSTRAINT [DF_ForumAnswers_LastModified] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ForumAnswers] ADD CONSTRAINT [PK_ForumAnswers] PRIMARY KEY CLUSTERED  ([ForumAnswerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ForumAnswers] ADD CONSTRAINT [FK_ForumAnswers_ForumQuestions] FOREIGN KEY ([ForumQuestionId]) REFERENCES [dbo].[ForumQuestions] ([ForumQuestionId])
GO
