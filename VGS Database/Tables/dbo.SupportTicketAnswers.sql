CREATE TABLE [dbo].[SupportTicketAnswers]
(
[SupportTicketAnswerId] [int] NOT NULL IDENTITY(1, 1),
[SupportTicketId] [int] NOT NULL,
[Description] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatorEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastModified] [datetime] NULL CONSTRAINT [DF_SupportTicketAnswers_LastModified] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SupportTicketAnswers] ADD CONSTRAINT [PK_SupportTicketAnswers] PRIMARY KEY CLUSTERED  ([SupportTicketAnswerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SupportTicketAnswers] ADD CONSTRAINT [FK_SupportTicketAnswers_SupportTickets] FOREIGN KEY ([SupportTicketId]) REFERENCES [dbo].[SupportTickets] ([SupportTicketId])
GO
