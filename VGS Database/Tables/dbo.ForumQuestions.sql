CREATE TABLE [dbo].[ForumQuestions]
(
[ForumQuestionId] [int] NOT NULL IDENTITY(1, 1),
[ForumTopicId] [int] NOT NULL,
[Header] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatorEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastModified] [datetime] NOT NULL CONSTRAINT [DF_ForumQuestions_LastModified] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ForumQuestions] ADD CONSTRAINT [PK_ForumQuestions] PRIMARY KEY CLUSTERED  ([ForumQuestionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ForumQuestions] ADD CONSTRAINT [FK_ForumQuestions_ForumTopics] FOREIGN KEY ([ForumTopicId]) REFERENCES [dbo].[ForumTopics] ([ForumTopicId])
GO
