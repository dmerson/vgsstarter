CREATE TABLE [dbo].[Faqs]
(
[FaqId] [int] NOT NULL IDENTITY(1, 1),
[FaqType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FaqQuestion] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FaqAnswer] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastModified] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Faqs_LastModified] DEFAULT ('Entered via Database'),
[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_Faqs_LastModifiedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Faqs] ADD CONSTRAINT [PK_Faqs] PRIMARY KEY CLUSTERED  ([FaqId]) ON [PRIMARY]
GO
